# Basic Fantasy GDScript

## Installation

1. Clone the repository into `addons`:

```bash
git clone --recurse-submodules git@gitlab.com:peter-kingsbury/basic-fantasy-gdscript.git
```

2. Add the file, `basic-fantasy-gdscript` to AutoLoads.

## Dependencies

A singleton called `Dice` which wraps [dice_syntax_gd`](https://github.com/oganm/dice_syntax_gd/tree/godot4)'s `roll()` function.

## Usage

TBD
