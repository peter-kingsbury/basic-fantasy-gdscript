extends Node


enum Sex {
	None = 0,
	Male = 1,
	Female = 2,
}

enum Race {
	None = 0,
	Human = 1,
	Elf = 2,
	Dwarf = 3,
	Halfling = 4,
}

enum Class {
	None = 0,
}

var fuzzy_damage = {
	"healthy": 8,
	"light": 9,
	"moderate": 10,
	"severe": 11,
	"critical": 12,
	"mortal": 13
}

var fuzzy_stats = {
	
}
