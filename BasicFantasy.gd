extends Node


var _dice
var _seed
var _rng

@onready var Constants = load("res://addons/basic-fantasy-gdscript/Constants.gd")


func initialize(dice, seed = 0, state = 0):
	# Store dice, seed
	_dice = dice
	_seed = seed
	# Create a new RNG
	_rng = RandomNumberGenerator.new()
	# Set the RNG seed
	_rng.seed = hash(_seed)
	# Restore the RNG state
	_rng.state = state
